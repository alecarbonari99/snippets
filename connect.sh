#!/bin/bash

# auto local device configurer

adb kill-server # server off

realPath=$(echo "$HOME")
while [[ -z $(echo $realPath | grep -E -o "([\/_a-zA-Z-]?*\/LowCodeTest)" | head -1) ]]
do
  # shellcheck disable=SC2154
  fReplacedPath=$(echo "$realPath" | sed "s/\\//./g")
  # shellcheck disable=SC2154
  sReplacedPath=$(echo "$fReplacedPath" | sed "s/$fReplacedPath/$fReplacedPath.*/g")
  # shellcheck disable=SC2154
  realPath=$(echo "$sReplacedPath" | sed "s/\./\\//g")
done

absPath=$(echo $realPath | grep -E -o "([\/_a-zA-Z-]?*\/LowCodeTest)" | head -1)

productModel=$(adb shell getprop ro.vendor.product.model)
versionRelease=$(adb shell getprop ro.build.version.release)
if [[ -n "$versionRelease" ]] && [[ -n "$productModel" ]]; then
  echo -e "\n Device: $productModel \n Version: $versionRelease"
fi

output=$(adb shell ip addr show wlan0)

if [[ -n "$output" ]]; then
  address=$(echo $output | grep -E -o 'inet\s[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | grep -E -o '([0-9]+[\.]?+)+' 2>/dev/null)
  pathAbs=$(echo "`pwd`"/src/main/resources/capabilities/androidCapabilities.json)
  androidCapabilities=$(cat "$absPath"/src/main/resources/capabilities/androidCapabilities.json | grep -E -o "([0-9]+[\.]?+)+" | xargs | sed "s/ /:/g")
  sed -i 's/'"$androidCapabilities"'/'"$address"':5556/g' "$absPath/src/main/resources/capabilities/androidCapabilities.json"
fi

fileOutput=$(cat $pathAbs)
if [[ -n "$fileOutput" ]]; then
  addressValidation=$(echo $fileOutput | grep -E -o $address)
  if [[ "$addressValidation" == "$address" ]]; then
    echo -e "\n Address $address configured successfully!\n"
  fi
fi

adb tcpip 5556 && adb connect $address:5556
