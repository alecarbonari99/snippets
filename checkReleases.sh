#!/bin/bash

# IGNORE, under test ~ phase 1 rdy !

repoReq=$(curl -s -i https://repo1.maven.org/maven2/io/lippia/core/ &)
lastRepoRelease=""
if [ $(echo $repoReq | grep -c "contents") -gt 0 ]
then
  releases=$(echo $repoReq | grep -E -o '>[0-9]+\.[0-9]+\.[0-9]?+\.?[0-9]?+/<')
  lastRepoRelease=$(echo $releases | awk -F '>' '{print $NF}' | awk -F '/' '{print $1}')
fi

dockerReq=$(curl -s -i https://hub.docker.com/v2/repositories/crowdar/lippia/tags/ &)

if [ $(echo $dockerReq | grep -c "\"name\"") -gt 0 ]
then
  releases=$(echo $dockerReq | grep -E -o '\"name\":\"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]?+')
  for release in $releases
  do
    versionDockerRelease=$(echo $release | awk -F '"' '{print $NF}')
    if [ $versionDockerRelease = $lastRepoRelease ]
    then
      lastDockerRelease=$versionDockerRelease
      echo -e "\nLippia-core dependency last release:\n"
      lastCoreDependency="<!-- https://mvnrepository.com/artifact/io.lippia/core -->
            \n<dependency>
            	\n<groupId>io.lippia</groupId>
            	\n<artifactId>root</artifactId>
            	\n<version>$lastRepoRelease</version>
            \n</dependency>"
      echo -e $lastCoreDependency
      echo -e "\nDocker Hub Lippia last release: \n"
      echo "$ docker pull crowdar/lippia:"$lastDockerRelease
      break
    fi
  done
  if [ -f ./pom.xml ]
  then
    settings=$(awk 'BEGIN {RS=""}{gsub(/\n/,"",$0); print$0}' ./pom.xml)
    echo -e "\nLast version released:" $lastRepoRelease
    currentlyVersions=$(echo $settings | grep -E -o '(<)\w+(>)(io\.lippia).+\s+.+(root)(<\/)\w+(>)\s(<)\w+(>)([0-9]?\.?)+(<\/)\w+(>)')
    currentlyCoreVersion=$(echo $currentlyVersions | grep -E -o '([0-9]?|[0-9]\.?)+')
    echo -e "\nCurrently version:" $currentlyCoreVersion
    if [ $lastRepoRelease = $currentlyCoreVersion ]
    then
      echo -e "\nUp to date !\n"
    else
      echo -e "\nNew version is available:" $lastRepoRelease
      echo "Are you want update Docker Hub and Dependencies ? - Yes[y] / No[n]"
      read resp
      if [ $resp = 'y' ]
      then
        # logic replace, update all
	# sed -i 's/$currentlyDependencyVersion/$lastCoreDependency/g' ./pom.xml // replace dependency
	sed -i 's/3.2.3.7/3.2.3.8/g' ./pom.xml # replace example
        echo -e "\nOld version: $currentlyCoreVersion ====> New version: $lastRepoRelease"
	mvn compile
      fi
    fi
  fi
else
  echo "No releases found!"
fi
