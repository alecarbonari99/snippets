package lippia.web.listeners;

import cucumber.api.TestCase;

import gherkin.pickles.Pickle;
import gherkin.pickles.PickleLocation;
import gherkin.pickles.PickleTag;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

public class ContextErrorInjector {

    private ContextErrorInjector() {}

    public static void tagInjector() {
        try {
            Class clazz = ClassUtils.getClass("cucumber.runtime.java.JavaHookDefinition$ScenarioAdaptor");
            Field fieldScenario = FieldUtils.getField(clazz, "scenario", true);
            fieldScenario.setAccessible(true);

            Object objectScenario = fieldScenario.get(ContextErrorModel.getScenario()); // setup from hooks

            Field fieldTestCase = objectScenario.getClass().getDeclaredField("testCase");
            fieldTestCase.setAccessible(true);

            TestCase testCase = (TestCase) fieldTestCase.get(objectScenario);
            Class testCaseFromRunner = ClassUtils.getClass("cucumber.runner.TestCase");
            Field pickleEvent = FieldUtils.getField(testCaseFromRunner, "pickleEvent", true);
            pickleEvent.setAccessible(true);

            Object objectPickleEvent =  pickleEvent.get(testCase);

            Field fieldPickle = objectPickleEvent.getClass().getDeclaredField("pickle");
            fieldPickle.setAccessible(true);

            Pickle pickle = (Pickle) fieldPickle.get(objectPickleEvent);

	    // tag injection in runtime ~! 
            pickle.getTags().add(new PickleTag(new PickleLocation(pickle.getLocations().get(0).getLine(), pickle.getLocations().get(0).getColumn()), ContextErrorModel.getContextError()));
            /* for other purposes ¿?
	    ~> List<String> sourceTags = pickle.getTags().stream().map(PickleTag::getName).collect(Collectors.toList());
            ~> ContextErrorModel.setTags(sourceTags);
	    */
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e.getCause());
        }
    }

}
