## No manual instantiation required

### Use like this (pom.xml)
```pom.xml
<cucumber.my.plugin> --plugin path.SampleCucumberHooks/Simpl: </cucumber.my.plugin>
<cucumber.options> X ${cucumber.my.plugin} </cucumber.options>
```