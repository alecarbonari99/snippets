package hooks;

import cucumber.api.event.*;
import cucumber.api.formatter.StrictAware;

import org.apache.commons.lang.NotImplementedException;

public class SampleCucumberHooks implements ConcurrentEventListener, StrictAware {

    public SampleCucumberHooks(String arg0) { // code (?!) }

    /* For cucumber plugins development */
    boolean strict = false;

    @Override
    public void setStrict(boolean strict) {
        this.strict = strict;
    }

    /* Event Handlers */
    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {

        eventPublisher.registerHandlerFor(WriteEvent.class,             event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(StepDefinedEvent.class,       event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(SnippetsSuggestedEvent.class, event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(EmbedEvent.class,             event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestRunStarted.class,         event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestSourceRead.class,         event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestCaseStarted.class,        event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestStepStarted.class,        event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestStepFinished.class,       event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestCaseFinished.class,       event -> {
            throw new NotImplementedException();
        });

        eventPublisher.registerHandlerFor(TestRunFinished.class,        event -> {
            throw new NotImplementedException();
        });

    }

}