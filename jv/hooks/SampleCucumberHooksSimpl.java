package hooks;

import cucumber.api.event.*;
import cucumber.api.formatter.StrictAware;

import org.apache.commons.lang.NotImplementedException;

public class SampleCucumberHooksSimpl implements ConcurrentEventListener, StrictAware {

    public SampleCucumberHooksSimpl(String arg0) { // code (?!) }

    /* For cucumber plugins development */
    boolean strict = false;

    @Override
    public void setStrict(boolean strict) {
        this.strict = strict;
    }

    /* Handlers are declared */
    private EventHandler<WriteEvent>             writeEventHandler             = this::handleWriteEvent;
    private EventHandler<StepDefinedEvent>       stepDefinedEventHandler       = this::handleStepDefinedEvent;
    private EventHandler<SnippetsSuggestedEvent> snippetsSuggestedEventHandler = this::handleSnippetsSuggestedEvent;
    private EventHandler<EmbedEvent>             embedEventHandler             = this::handleEmbedEvent;
    private EventHandler<TestRunStarted>         testRunStartedHandler         = this::handleTestRunStarted;
    private EventHandler<TestSourceRead>         testSourceReadHandler         = this::handleTestSourceRead;
    private EventHandler<TestCaseStarted>        caseStartedHandler            = this::handleTestCaseStarted;
    private EventHandler<TestStepStarted>        stepStartedHandler            = this::handleTestStepStarted;
    private EventHandler<TestStepFinished>       stepFinishedHandler           = this::handleTestStepFinished;
    private EventHandler<TestCaseFinished>       caseFinishHandler             = this::handleTestCaseFinished;
    private EventHandler<TestRunFinished>        runFinishedHandler            = this::handleRunFinished;

    /* Handlers are registered */
    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(WriteEvent.class,             writeEventHandler);
        eventPublisher.registerHandlerFor(StepDefinedEvent.class,       stepDefinedEventHandler);
        eventPublisher.registerHandlerFor(SnippetsSuggestedEvent.class, snippetsSuggestedEventHandler);
        eventPublisher.registerHandlerFor(EmbedEvent.class,             embedEventHandler);
        eventPublisher.registerHandlerFor(TestRunStarted.class,         testRunStartedHandler);
        eventPublisher.registerHandlerFor(TestSourceRead.class,         testSourceReadHandler);
        eventPublisher.registerHandlerFor(TestCaseStarted.class,        caseStartedHandler);
        eventPublisher.registerHandlerFor(TestStepStarted.class,        stepStartedHandler);
        eventPublisher.registerHandlerFor(TestStepFinished.class,       stepFinishedHandler);
        eventPublisher.registerHandlerFor(TestCaseFinished.class,       caseFinishHandler);
        eventPublisher.registerHandlerFor(TestRunFinished.class,        runFinishedHandler);
    }

    public void handleWriteEvent(WriteEvent event) {
        throw new NotImplementedException();
    }

    public void handleStepDefinedEvent(StepDefinedEvent event) {
        throw new NotImplementedException();
    }

    public void handleSnippetsSuggestedEvent(SnippetsSuggestedEvent event) {
        throw new NotImplementedException();
    }

    public void handleEmbedEvent(EmbedEvent event) {
        throw new NotImplementedException();
    }

    public void handleTestRunStarted(TestRunStarted event) {
        throw new NotImplementedException();
    }

    public void handleTestSourceRead(TestSourceRead event) {
        throw new NotImplementedException();
    }

    public void handleTestCaseStarted(TestCaseStarted event) {
        throw new NotImplementedException();
    }

    public void handleTestStepStarted(TestStepStarted event) {
        throw new NotImplementedException();
    }

    public void handleTestStepFinished(TestStepFinished event) {
        throw new NotImplementedException();
    }

    public void handleTestCaseFinished(TestCaseFinished event) {
        throw new NotImplementedException();
    }

    public void handleRunFinished(TestRunFinished event) {
        throw new NotImplementedException();
    }

}