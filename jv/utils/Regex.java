package <?>;

import org.intellij.lang.annotations.Language;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is intended to make it easy to use regular expressions and check or get (the existence of) a subsequence
 * **/
public class Regex {

    private Regex() {}

    private static Matcher applyMatcher(String regex, String sequence) {
        return Pattern.compile(regex).matcher(sequence);
    }

    /**
     * @param regex: regular expression
     * @param sequence: sequence to search
     * @return true if there is at least one match
     * **/
    public static boolean find(@Language("RegExp") String regex, String sequence) {
        return finder((reg, seq) -> applyMatcher(reg, seq).find(), regex, sequence);
    }

    private static boolean finder(BiPredicate<String, String> predicate, String regex, String sequence) {
        return predicate.test(regex, sequence);
    }

    /**
     * @param regex: regular expression
     * @param sequence: sequence to search
     * @return the (possibly empty) subsequence matched by the previous match
     * **/
    public static String search(@Language("RegExp") String regex, String sequence) {
        return search((reg, seq) -> {
            Matcher matcher = applyMatcher(reg, seq);
            if (matcher.find()) return matcher.group();
            throw new RuntimeException("Sequence not found, regex passed as parameter: " + reg);
        }, regex, sequence);
    }

    private static String search(BiFunction<String, String, String> function, @Language("RegExp") String regex, String sequence) {
        return function.apply(regex, sequence);
    }

}