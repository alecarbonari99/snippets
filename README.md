# SNIPPETS 

## connect.sh @_Unreleased_

This file provides an automatic local device connection over a wireless connection to run automated tests, as well as automatic configuration of the necessary capabilities and device parameters.   
Please note that at the moment it only works for the Lippia Framework and is not supported on iOS.

### Requirements
- Not include

### Future features
- Flags Support ( optional )
- Parameters Support ( optional )
- iOS Support
- All Frameworks Support 
- Automatic dockerization by profile specified
- Appium controller
- Linking autoupdate dependencies, docker images, and vagrant boxes. Includes:
    - Process execution timing
    - Automatic code update according to the differences between the possible versions
- Handle IDE for nicer and more configurable logs without the need to go to a shell

## Future Desktop Application

### Requirements
- Not include

### Features
- Elements inspector
- Hook and browser process clone to modify the tree to our liking to remove studio dependency. Includes:   
    - Ability to set attributes to elements
    - Locate elements as your prefer in your tests
- . . .
